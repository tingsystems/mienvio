#!/usr/bin/python
# coding: utf-8
# (c) 2018 Tingsystems <@tingsystems>

from requests import request

try:
    import json
except ImportError:
    import simplejson as json

__version__ = '0.0.1'
__author__ = 'Tingsystems'

sandbox = False

api_key = None


class MienvioError(Exception):
    def __init__(self, error_json):
        super(MienvioError, self).__init__(error_json)
        self.error_json = error_json


class MalformedRequestError(MienvioError):
    pass


class AuthenticationError(MienvioError):
    pass


class ProcessingError(MienvioError):
    pass


class ResourceNotFoundError(MienvioError):
    pass


class ParameterValidationError(MienvioError):
    pass


class ApiError(MienvioError):
    pass


class Mienvio:
    """
    Build request mienvio API
    """

    first_key = None

    @classmethod
    def build_http_request(cls, method, path, payload=None, params=None):
        """

        :param method: get, post, patch, put
        :param path: resource in the Mienvio API
        :param payload: request body
        :param params: query params by url
        :return:
        """
        api_base = 'http://pruebas.mienvio.mx/api/' if sandbox else 'https://production.mienvio.mx/api/'

        headers = {
            'Authorization': 'Bearer {}'.format(api_key),
            'content-type': 'application/json'
        }
        print(headers, 'headers')
        print(api_base, 'api_base')
        print(path, 'path')
        print(payload, 'payload')

        try:
            body = request(
                method, '{}{}'.format(api_base, path), data=json.dumps(payload), params=params, headers=headers
            )
        except Exception:
            raise ApiError({'error': 'Service not available'})

        if body.status_code == 200 or body.status_code == 201 or body.status_code == 204:
            return body.json()
        if body.status_code == 400:
            raise MalformedRequestError(body.json())
        elif body.status_code == 401:
            raise AuthenticationError(body.json())
        elif body.status_code == 402:
            raise ProcessingError(body.json())
        elif body.status_code == 404:
            raise ResourceNotFoundError(body.json())
        elif body.status_code == 422:
            raise ParameterValidationError(body.json())
        elif body.status_code == 500:
            raise ApiError(body.json())
        else:
            raise MienvioError(body.json())

    @classmethod
    def to_object(cls, response, first_key=None):
        data = response[first_key] if first_key else response
        for key, value in data.items():
            setattr(cls, key, value)
        return cls

    @classmethod
    def create(cls, data):
        """

        :param data: dict with data for create object
        :return: object with data from response
        """
        return cls.to_object(cls.build_http_request('post', cls.__name__.lower(), data), cls.first_key)

    @classmethod
    def retrieve(cls, oid, params=None):
        """

        :params oid: id of object retrieve
        :return: object with data from response
        """
        return cls.to_object(
            cls.build_http_request('get', '{}/{}'.format(cls.__name__.lower(), oid), params=params), cls.first_key
        )

    @classmethod
    def all(cls, params=None):
        """
        :type params: extra params for build request
        :return: list of objects from response mienvio api
        """
        return cls.build_http_request('get', cls.__name__.lower(), params=params)

    @classmethod
    def query(cls, params=None):
        """
        :type params: extra params for build request
        :return: list of objects from response mienvio api
        """
        return cls.build_http_request('get', cls.__name__.lower(), params=params)

    @classmethod
    def update(cls, data, oid):
        """
        :param oid: id object
        :type data: data
        :return: object with data from response
        """
        return cls.to_object(
            cls.build_http_request('put', '{}/{}'.format(cls.__name__.lower(), oid), data), cls.first_key
        )

    @classmethod
    def delete(cls, oid):
        """
        :param oid: id object
        :return: None
        """
        return cls.build_http_request('delete', '{}/{}'.format(cls.__name__.lower(), oid))


class Addresses(Mienvio):
    """
    CRUD Addresses
    """
    first_key = 'address'


class Shipments(Mienvio):
    """
    CRUD Shipments
    """
    first_key = 'shipment'

    @classmethod
    def rates(cls, shipment_id):
        return cls.build_http_request('get', '{}/{}/rates'.format(cls.__name__.lower(), shipment_id))


class Tracking(Mienvio):
    first_key = 'tracking'

    @classmethod
    def get(cls, tracking_code, provider):
        return cls.to_object(
            cls.build_http_request('get', '{}/{}/{}'.format(cls.__name__.lower(), tracking_code, provider)),
            cls.first_key
        )


class Purchases(Mienvio):
    """
    CRUD Purchases
    """
    first_key = 'purchase'


class Orders(Mienvio):
    """
    CRUD orders
    """
    first_key = 'order'
