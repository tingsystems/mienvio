# Mienvio
Mienvio python wrapper http://api.mienvio.mx

Install
```sh
pip install -e git+https://gitlab.com/tingsystems/mienvio.git@master#egg=mienvio
```

NOTE: The Wrapper by default use api base of Mienvio

## Library Development and Testing

You can test the mienvio library with nose from the mienvio library root:

## Create addresses

```python
import mienvio

mienvio.api_key = 'api_key'
mienvio.sandbox = True

address_object = {
    "object_type": "FROMTO",
    "name": "Cirilo",
    "street": "Salustio Amezcua #7 int 1",
    "street2": "Blvd. Lazaro Cardenas",
    "reference ": "Puerta blanca a lado de taller de llaves",
    "zipcode": "59514",
    "email": "frida@xxx.com",
    "phone": "3333333333",
    "bookmark": False,
    "alias": "Casa de dante",
}

address = mienvio.Addresses.create(address_object)
print(address.object_id)
```

## Create shipment quote

```python
import mienvio

mienvio.api_key = 'api_key'
mienvio.sandbox = True

shipment_quote_object = {
    "object_purpose": "QUOTE",
    "zipcode_from": 59033,
    "zipcode_to": 59514,
    "weight": 5,
    "length": 3.1,
    "height": 5.0,
    "width": 3.1,
    "description": "Test",
}

shipment = mienvio.Shipments.create(shipment_quote_object)
rates = mienvio.Shipments.rates(shipment.object_id)
print(rates)
```

## Create shipment purchase

```python
import mienvio

mienvio.api_key = 'api_key'
mienvio.sandbox = True

shipment_purchase_object = {
    "object_purpose": "PURCHASE",
    "address_from": 57608,
    "address_to": 57609,
    "weight": 5,
    "length": 3.1,
    "height": 5.0,
    "width": 3.1,
    "description": "Test",
    "rate": 952
}
# create shipment type purchase
shipment = mienvio.Shipments.create(shipment_purchase_object)

purchase_object = {
    "shipments": [shipment.object_id]
}
# create purchase
purchase = mienvio.Purchases.create(purchase_object)
```

## Get data

```python
import mienvio

mienvio.api_key = 'api_key'
mienvio.sandbox = True

# get objects list

addresses = mienvio.Addresses.all(params={'limit': 10, 'page': 1})
shipments = mienvio.Shipments.all(params={'limit': 10, 'page': 1})
purchases = mienvio.Purchases.all(params={'limit': 10, 'page': 1})

# Retrieve object

address = mienvio.Addresses.retrieve('object_id_address')
shipment = mienvio.Shipments.retrieve('object_id_shipment')
purchase = mienvio.Purchases.retrieve('object_id_purchase')

```


```sh
For TestCase 
$ nosetests addresses.py
```
