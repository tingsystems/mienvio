import unittest
import random
import mienvio


class BaseEndpointTestCase(unittest.TestCase):
    random.seed()

    client = mienvio

    address_object = {
        "object_type": "FROMTO",
        "name": "Cirilo",
        "street": "Salustio Amezcua #7 int 1",
        "street2": "Blvd. Lazaro Cardenas",
        "reference ": "Puerta blanca a lado de taller de llaves",
        "zipcode": "59514",
        "email": "frida@xxx.com",
        "phone": "3333333333",
        "bookmark": False,
        "alias": "Casa de dante",
    }

    shipment_quote_object = {
        "object_purpose": "QUOTE",
        "zipcode_from": 59033,
        "zipcode_to": 59514,
        "weight": 5,
        "length": 3.1,
        "height": 5.0,
        "width": 3.1,
        "description": "Test",
    }

    shipment_purchase_object = {
        "object_purpose": "PURCHASE",
        "address_from": 57608,
        "address_to": 57609,
        "weight": 5,
        "length": 3.1,
        "height": 5.0,
        "width": 3.1,
        "description": "Test",
        "rate": 952
    }

    purchase_object = {
        "shipments": [37021]
    }
