from . import BaseEndpointTestCase


class ShipmentsEndPintTestCase(BaseEndpointTestCase):

    def test_create_purchase(self):
        self.client.api_key = 'api_key'
        self.client.sandbox = True
        shipment = self.client.Purchases.create(self.purchase_object)
        assert shipment.object_id

    def test_get_all_purchase(self):
        self.client.api_key = 'api_key'
        self.client.sandbox = True
        shipments = self.client.Purchases.all()
        assert len(shipments) > 0
