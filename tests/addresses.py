from . import BaseEndpointTestCase


class AddressesEndPintTestCase(BaseEndpointTestCase):

    def test_create_address(self):
        self.client.api_key = 'api_key'
        self.client.sandbox = True
        address = self.client.Addresses.create(self.address_object)
        assert address.object_id

    def test_get_all_address(self):
        self.client.api_key = 'api_key'
        self.client.sandbox = True
        addresses = self.client.Addresses.all()
        assert len(addresses) > 0
