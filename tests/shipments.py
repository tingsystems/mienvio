from . import BaseEndpointTestCase


class ShipmentsEndPintTestCase(BaseEndpointTestCase):

    def test_create_shipment_quote(self):
        self.client.api_key = 'api_key'
        self.client.sandbox = True
        shipment = self.client.Shipments.create(self.shipment_quote_object)
        assert shipment.object_id

    def test_create_shipment_purchase(self):
        self.client.api_key = 'api_key'
        self.client.sandbox = True
        shipment = self.client.Shipments.create(self.shipment_purchase_object)
        assert shipment.object_id

    def test_get_all_shipment_rates(self):
        self.client.api_key = 'api_key'
        self.client.sandbox = True
        rates = self.client.Shipments.rates(37014)
        assert len(rates) > 0

    def test_get_all_shipment(self):
        self.client.api_key = 'api_key'
        self.client.sandbox = True
        shipments = self.client.Shipments.all()
        assert len(shipments) > 0
